﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Composites
{
    public class CompositeObjectID
    {
        Int32 _id;

        public CompositeObjectID()
        {
            ObjectID = Guid.NewGuid().ToString();
            _id = 0;

        }

        public String ObjectID { get; set; }

        public Int32 CompositeID { get { return _id; } set { _id = value; } }
    }
}
