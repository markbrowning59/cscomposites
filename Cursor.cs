﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Composites
{
    public class Cursor : Composite
    {
        public String Type { get; set; }
        private List<IComposite> _cursorList;

        public Cursor(String type, List<IComposite> list) { this.Type = type; CurrentPosition = 0; _cursorList = new List<IComposite>(list); }

        public Int64 CurrentPosition { get; set; }
        public Int64 Next() 
        {
            if (++CurrentPosition > _cursorList.Count - 1)
                --CurrentPosition;

            return CurrentPosition;
       }


        public Int64 Previous()
        {
            if (--CurrentPosition < 0)
                ++CurrentPosition;

            return CurrentPosition;
        }


    }
}
