﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Composites
{
    public interface IComposite
    {
        String CompositeType { get; set; }
        String ObjectID { get; set; }
        Int32 CompositeID { get; set; }
        String Type { get; set; }
        String Name { get; set; }
        String Value { get; set; }
        String Description { get; set; }
        Boolean Active { get; set; }
        Boolean LazyLoad { get; set; }

        //Parents       
        IList<IComposite> Parents { get; }

        void insertParent(IComposite parent, Boolean addChildToParent = true);


        void removeParent(IComposite parent, Boolean removeChild = true);
        IComposite getParent(String objectId);
      
        void clearParentList(Boolean removeChild = true);


        //Children
        IList<IComposite> Children { get; }
        
        void addChild(IComposite child);
        void removeChild(IComposite child, Boolean removeParent = true);
        void removeChild(Identifier id, Boolean removeParent = true);

        void moveChild(IComposite fromParent, IComposite toParent);

        IComposite getChild(String objectId);

        void clearChildList();

        IList<IComposite> getChildren(Identifier id);

        //Iterator            
        List<T> getSkipList<T>();
        List<IComposite> getSkipList(String type, String name = null, String value = null);

        //Persistance
        int write(IDataTransfer DataTransfer);
        void read(IDataTransfer DataTransfer, Boolean lazyLoad = true);

        //Identifiers
        List<Identifier> Identifiers { get; }
        
        IList<Identifier> GetIdentifier(String value, String name = null, String type = null);
        void AddIdentifier(Identifier identifier);
        void RemoveIdentifier(Identifier identifier);

        //Attributes
        IList<CompositeAttribute> Attributes { get; }
        void AddAttribute(CompositeAttribute attribute);
        void AddAttribute(CompositeAttributeDefinition attDef);
        void AddAttribute(String name, String value, String type = null);

        int SetAttribute(String name, String type, String value );

        void RemoveAttribute(String name, String type = null);
        void RemoveAttribute(CompositeAttribute attribute); 
        CompositeAttribute GetAttribute(String name, String type = null);
        void InitializeAttributes();



        //Attribute Definitions
        IList<CompositeAttributeDefinition> AttributeDefinitions { get; }

        CompositeAttributeDefinition AddAttributeDefinition(String name, String type, String value = "", Boolean writeTodisk = true,  Boolean applyAtInstance = false, Boolean inherit = true);        
        void AddAttributeDefinition(CompositeAttributeDefinition attDef);
        void RemoveAttributeDefinition(String name, String type = null);
        void RemoveAttributeDefinition(CompositeAttributeDefinition attDef);
        IList<CompositeAttributeDefinition> GetAttributeDefinition(String name, String type = null);


        //IsEqual
        //Is Equal by type
        Boolean isEqual(IComposite com);

        //Is Equal By Identifier
        Boolean isEqual(Identifier id);

        Boolean isEqual(String type, String name = null, String value = null);
   

    }
}
