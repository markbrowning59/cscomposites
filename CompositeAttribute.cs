﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Serialization;

namespace Composites
{

    [DataContract(IsReference = true)]
    public class CompositeAttribute : Composite
    {
        [DataMember]
        public Boolean WriteToDisk { get; set; }

        public CompositeAttribute() {  WriteToDisk = true; }

        public CompositeAttribute(String name, String type, String value = null) : this()
        {            
            Name = name;
            Type = type;
            Value = value;
          
        }
    }
}
