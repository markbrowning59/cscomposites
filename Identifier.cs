﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Serialization;


namespace Composites
{

    [DataContract(IsReference= true)]

    public class Identifier : Composite
    {


        #region properties

             #region datacontract

               [DataMember]
                     public Identifier ParentIdentifier { 
                     
                         get { return this.Parents.OfType<Identifier>().FirstOrDefault(); }

                         //Set has to be present if the property is used as a Data Member within a Data Contract
                         set { throw new Exception("ParentIdentifier is Read Only"); }
                     
                     }

               [DataMember]
                    public Identifier ChildIdentifier { 
                    
                        get { return this.Identifiers.FirstOrDefault(); }

                   //Set has to be present if the property is used as a Data Member within a Data Contract
                        set { throw new Exception("ChildIdentifier is Read Only"); }
                    }

               [DataMember]
               public Int32 ParentID
               {
                   get { return Parents.OfType<Identifier>().Count() > 0 ? Parents.OfType<Identifier>().FirstOrDefault().CompositeID : 0; }

                   //Set has to be present if the property is used as a Data Member within a Data Contract
                   set { throw new Exception("ParentID is Read Only"); }

               }
      
             #endregion

        #endregion
        
               
        public override Boolean isEqual(Identifier id)
        {
            Boolean isEqual = (this.Type.ToUpper().Trim() == id.Type.ToUpper().Trim() && this.Name.ToUpper().Trim() == id.Name.ToUpper().Trim() && this.Value.ToUpper().Trim() == id.Value.ToUpper().Trim());

            if (isEqual)
                foreach (Identifier LID in id.Identifiers)
                    foreach (Identifier RID in this.Identifiers)
                    {
                        isEqual = LID.isEqual(RID);

                        if (isEqual)
                            return isEqual;
                    }



            return isEqual;

        }

        public override Boolean isEqual(String type, String name = null, String value = null)
        {

            if (String.IsNullOrEmpty(name) && String.IsNullOrEmpty(value))
                return (this.Type.ToUpper().Trim() == type.ToUpper().Trim());
            else if (String.IsNullOrEmpty(value))
                return (this.Type.ToUpper().Trim() == type.ToUpper().Trim() && this.Name.ToUpper().Trim() == name.ToUpper().Trim());
            else
                return (this.Type.ToUpper().Trim() == type.ToUpper().Trim() && this.Name.ToUpper().Trim() == name.ToUpper().Trim() && this.Value.ToUpper().Trim() == value.ToUpper().Trim());

        }

        //Identifiers
        public override void AddIdentifier(Identifier identifier)
        {
            if(this.Identifiers.Count == 0)
                base.AddIdentifier(identifier);
        }
  
        
        public Int32 ParentIdentifierID { get; set; }
       
        public Int32 ChildIdentifierID { get; set; }
      
        public Identifier getRootIdentifier()
        {
            Identifier parentIdentifer = this;

            if (this.ParentIdentifier == null)
                return this;

            while (parentIdentifer.ParentIdentifier != null)
                parentIdentifer = parentIdentifer.ParentIdentifier;


            return parentIdentifer;
        }


    }
}
