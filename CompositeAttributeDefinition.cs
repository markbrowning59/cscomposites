﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Serialization;


namespace Composites
{
    [DataContract(IsReference = true)]

    public class CompositeAttributeDefinition : Composite
    {
        [DataMember]
        public Boolean WriteToDisk { get; set; }
        
        [DataMember]
        public Boolean Inherit { get; set; }
        
        [DataMember]
        public Boolean ApplyAtInstance { get; set; }

        public CompositeAttributeDefinition() { Value = String.Empty; }

        public CompositeAttributeDefinition(String name, String type, Boolean writeToDisk = true, Boolean inherit = true) 
            : this()
        {           

            Name = name;
            Type = type;            
            Inherit = inherit;
            WriteToDisk = writeToDisk;
        }
    }
}
