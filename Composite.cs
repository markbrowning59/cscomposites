﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Composites
{

    [DataContract(IsReference = true)]
    public class Composite : IComposite
    {

////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Properties
////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region properties

             #region DataContract
                            
             //Object ID             
                [NonSerialized]
                   private CompositeObjectID _objectID;
                
                [DataMember]
                   public String ObjectID { get { return _objectID.ObjectID; } set { _objectID.ObjectID = value; } }

                [DataMember]
                   public Int32 CompositeID { get { return _objectID.CompositeID; } set { _objectID.CompositeID = value; } }
                 
        
            //Active               
                [NonSerialized]
                   private Boolean _active;
                  
                [DataMember]
                   public Boolean Active
                 {
                     get { return _active; }
                     set
                     {
                         _active = value;

                         foreach (Composite com in this.Children)
                             com.Active = _active;
                     }
                 }

           //Name-Value Pair 
                [DataMember]
                    virtual public String Type { get; set; }

                [DataMember]
                    virtual public String Name { get; set; }

                [DataMember]
                    virtual public String Value { get; set; }

                [DataMember]
                    virtual public String Description { get; set; }
            
                [DataMember]
                    public Boolean LazyLoad { get; set; }

                [DataMember]
                    public String CompositeType { get; set; }

                [DataMember]
                    public List<Identifier> Identifiers { get { return _identifiers; } }

                [NonSerialized]
                    private List<Identifier> _identifiers;


                //Attribute Definitions and Attributes      
                [DataMember]
                public virtual IList<CompositeAttributeDefinition> AttributeDefinitions
                {

                    get
                    {
                        IList<CompositeAttributeDefinition> attDefList = new List<CompositeAttributeDefinition>();

                        foreach (Composite com in Parents)
                            foreach (CompositeAttributeDefinition attDef in com.AttributeDefinitions.Where(e => e.Inherit == true))
                                attDefList.Add(attDef);

                        foreach (CompositeAttributeDefinition attDef in getSkipList<CompositeAttributeDefinition>())
                            attDefList.Add(attDef);

                        return attDefList;

                    }
                }

                [DataMember]
                public virtual IList<CompositeAttribute> Attributes
                {

                    get { return getSkipList<CompositeAttribute>(); }


                }



             #endregion
          
            public IList<IComposite> Parents { get { return _parents.AsReadOnly(); } }            
            private List<IComposite> _parents;

            public IList<IComposite> Children { get { return _children.AsReadOnly(); } }
            private List<IComposite> _children;




       #endregion


////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Constructors
////////////////////////////////////////////////////////////////////////////////////////////////////////////

       #region constructors

            public Composite(Identifier identifier, Boolean lazyLoad = true) : this() { AddIdentifier(identifier); LazyLoad = lazyLoad; }
            public Composite(Boolean lazyLoad) : this() { LazyLoad = lazyLoad; }
            
            public Composite()
        {

            LazyLoad = true;

            _parents = new List<IComposite>();
            _children = new List<IComposite>();
            _identifiers = new List<Identifier>();

            CompositeType = this.GetType().Name;

            _objectID = new CompositeObjectID();
            CompositeID = 0;

            _active = true;

            Description = String.Empty;

            Written = false;
            Read = false;

            Value = String.Empty;


        }

       #endregion


////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Parents
////////////////////////////////////////////////////////////////////////////////////////////////////////////

        #region parents


            //This function inserts a parent composite into the child composite
            //A composite can have multiple parents but the same parent should not be added twice
            //It also adds this child composite to the parent's child list when addChidToParent is True
            public void insertParent(IComposite parent, Boolean addChildToParent = true)
        {

            if (getParent(parent.ObjectID) == null)
                _parents.Add(parent);

            if (addChildToParent && parent.getChild(this.ObjectID) == null)
                parent.addChild(this);
                        
        }


            //This function removes the parent from the child object..
            //If removeChild is true then the child is removed from the parent object as well
            public void removeParent(IComposite parent, Boolean removeChild = true)
        {


            try
            {
                if (_parents.Contains(parent))
                {
                    if (removeChild)
                        parent.removeChild(this, false);

                    _parents.Remove(parent);

                }


            }
            catch (Exception e) { }


        }

            //Function to get a child's parent by objectId..
            public IComposite getParent(String objectId)
        {
            foreach (IComposite parent in _parents)
                if (parent.ObjectID == objectId)
                    return parent;

            return null;
        }

            //Removes all parents from composite's parent list
            //If removechild is true then the child is removed from parent 
            public void clearParentList(Boolean removeChild = true)
        {
            List<IComposite> parents = new List<IComposite>(_parents);
            foreach (IComposite p in parents)
                this.removeParent(p, removeChild);

        }

        #endregion


 ////////////////////////////////////////////////////////////////////////////////////////////////////////////
 /// Children
 ////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
#region children          



            //Adds a child composite to this child list        
            public void addChild(IComposite child)
        {
            if (!Active)
                return;
            try
            {
                if (child.Equals(this))
                    return;

                if (!_children.Contains(child))
                {
                    //As the addchild function adds a child to a parent then
                    //AddChildToParent is set to false. 
                    child.insertParent(this, false);              
                
                    _children.Add(child);
                }


            }
            catch (Exception e) { }

        }


            //Removes a child from the parent composite
            //If removeParent is false then the parent will not be removed from the child. 
            //But the child will be removed from the parent composite
            public void removeChild(IComposite child, Boolean removeParent = true)
        {
            try
            {
                if (_children.Contains(child))
                {
                    if (removeParent)
                        child.removeParent(this, false);

                    _children.Remove(child);

                }

            }
            catch (Exception e) { }
        }


            //Removes a child from the parent composite using an identifier
            //If removeParent is false then the parent will not be removed from the child. 
            //But the child will be removed from the parent composite
            public void removeChild(Identifier id, Boolean removeParent = true)
        {
            IList<IComposite> removeList = getChildren(id);

            foreach (IComposite com in removeList)
                removeChild(com, removeParent);

        }


            //Gets a composite's child by objectId
            public IComposite getChild(String objectId)
        {
            foreach (IComposite child in _children)
                if (child.ObjectID == objectId)
                    return child;

            return null;
        }


            //Returns a list of children composites that contain the id Identifier
            public IList<IComposite> getChildren(Identifier id)
        {
            List<IComposite> returnList = new List<IComposite>();


            foreach (Composite com in this.Children)
                if (com.isEqual(id))
                    returnList.Add(com);

            return returnList;

        }


            //Returns a list of child composites based on and Identifier and type
            public IList<T> getChildren<T>(Identifier id)
        {
            List<T> returnList = new List<T>();

            foreach (Composite com in this.Children)
                if (com.isEqual(id) && com.GetType() == typeof(T))
                    returnList.Add((T)Convert.ChangeType(com, typeof(T)));

            return returnList;

        }


            //Moves a child component from one parent to another
            public void moveChild(IComposite fromParent, IComposite toParent)
        {
            fromParent.removeChild(this);
            toParent.addChild(this);
        }


            //Removes all children form parent composite
            public void clearChildList()
        {
            
                List<IComposite> children = new List<IComposite>(_children);
                foreach (IComposite c in children)
                    this.removeChild(c);

            
        }


       #endregion



////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Skip List
////////////////////////////////////////////////////////////////////////////////////////////////////////////
       #region SkipList
         public List<T> getSkipList<T>()
        {
            List<T> returnList = new List<T>();

            foreach (IComposite child in _children)
                if (child.GetType() == typeof(T))
                    returnList.Add((T)child);

            return returnList;
        }
         public List<IComposite> getSkipList(string type, string name = null, string value = null)
        {

            if (String.IsNullOrEmpty(name) && String.IsNullOrEmpty(value))
                return Children.Where(e => e.Type == type).ToList<IComposite>();
            else if (String.IsNullOrEmpty(value))
                return Children.Where(e => e.Type == type && e.Name == name).ToList<IComposite>();
            else
                return Children.Where(e => e.Type == type && e.Name == name && e.Value == value).ToList<IComposite>();

        }


       #endregion


////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Persistance
////////////////////////////////////////////////////////////////////////////////////////////////////////////
       #region Persistance
            virtual public int write(IDataTransfer DataTransfer) { return DataTransfer.writeItem(this); }
            virtual public void read(IDataTransfer DataTransfer, bool lazyLoad = true) { DataTransfer.readItem(this, lazyLoad); }
            public Boolean Written { get; set; }
            public Boolean Read { get; set; }
      #endregion



////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Idenfitiers
////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region Idenfitiers          


            virtual public void AddIdentifier(Identifier identifier) { identifier.insertParent(this); _identifiers.Add(identifier); }


            public void RemoveIdentifier(Identifier identifier) { _identifiers.Remove(identifier); }


            //Returns a list of identifiers match a combination of name, type, value
            public IList<Identifier> GetIdentifier(String name, String type = null, String value = null)
        {

            if (String.IsNullOrEmpty(type) && String.IsNullOrEmpty(value))
                return this.Identifiers.Where(e => e.Name == name).ToList<Identifier>();
            else if (String.IsNullOrEmpty(value))
            {
                return this.Identifiers.Where(e => e.Type == type && e.Name == name).ToList<Identifier>();
            }
            else
                return this.Identifiers.Where(e => e.Type == type && e.Name == name && e.Value == value).ToList<Identifier>();
        }
            
            public void clearIdentifiers()
        {           
            List<Identifier> identifiers = new List<Identifier>(Identifiers);

            foreach (Identifier id in identifiers)
                this.RemoveIdentifier(id);
                    
        }

        #endregion

            
////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Is Equal
////////////////////////////////////////////////////////////////////////////////////////////////////////////
       #region IsEqual 
       
             public virtual Boolean isEqual(IComposite com)
        {

            Boolean isEqual = false;

            if (com == this)
                return true;

            foreach (Identifier LID in com.Identifiers)
                foreach (Identifier RID in this.Identifiers)
                {
                    isEqual = LID.isEqual(RID);

                    if (isEqual)
                        return isEqual;
                }

            return isEqual;


        }

             public virtual Boolean isEqual(Identifier id)
        {
            Boolean isEqual = false;

            foreach (Identifier RID in this.Identifiers)
            {
                isEqual = id.isEqual(RID);
                if (isEqual)
                    break;
            }

            return isEqual;


        }

             //This function only compares the top level identifier using a combination of name, type and value... 
             //If type and value is null it only checks name.. 
             //If value is null it compares name and type
             //Otherwise it compares Name, Type and Value.
             public virtual Boolean isEqual(String name , String type = null, String value = null)
        {

            foreach (Identifier id in Identifiers)
            {
                if (String.IsNullOrEmpty(type) && String.IsNullOrEmpty(value))
                {
                    if (id.Name == name)
                        return true;
                }
                else if (String.IsNullOrEmpty(value))
                {

                    if (id.Type == type && id.Name == name)
                        return true;

                }
                else
                {
                    if (id.Type == type && id.Name == name && id.Value == value)
                        return true;
                }
            
            }

            return false;
        }

        #endregion


 ////////////////////////////////////////////////////////////////////////////////////////////////////////////
 ///Attribute Definitions
 ////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region AttributeDefinitions

            //Recuring calls to parent's AttributeDefinition property to get inherted definitions    
        



             //Adds an attribute definition to the composite
             //There can only be one definitionwith the sane Name and Type
             //Value is the attribute's  default value
             //Inhert specifies if the composite's children inherits the attribute definition

        
             public virtual CompositeAttributeDefinition AddAttributeDefinition(String name, String type, String value = "", 
                 Boolean inherit = true, Boolean applyAtInstance = false, Boolean writeTodisk = true)
        {

            CompositeAttributeDefinition attDef = null;

            if (GetAttributeDefinition(name, type).Count < 1)
            {
                attDef = new CompositeAttributeDefinition(name, type) { Value = value, Inherit = inherit, 
                    ApplyAtInstance = applyAtInstance, WriteToDisk = writeTodisk };

                addChild(attDef);
            }

            return attDef;

        }


             //Adds a definition to a composite
             public virtual void AddAttributeDefinition(CompositeAttributeDefinition attDef)
        {

            if (GetAttributeDefinition(attDef.Name, attDef.Type).Count < 1)
                addChild(attDef);

        }

             //Removes Attribute Defintions based on name and type
             //If type is null then only Name is used  
             public virtual void RemoveAttributeDefinition(String name, String type = null)
        {

            foreach (CompositeAttributeDefinition attDef in GetAttributeDefinition(name, type))
                removeChild(attDef);
        }


             //Removes Attribute Defintions based on definition       
             public virtual void RemoveAttributeDefinition(CompositeAttributeDefinition attDef) { removeChild(attDef); }


             //Returns a list of Attribute Defintions based on name and type
             //If type is null then only Name is used       
             public IList<CompositeAttributeDefinition> GetAttributeDefinition(String name, String type = null)
        {

            if (String.IsNullOrEmpty(type))
            {
                return this.AttributeDefinitions.Where(e => e.Name == name).ToList<CompositeAttributeDefinition>();
            }
            else
                return this.AttributeDefinitions.Where(e => e.Type == type && e.Name == name).ToList<CompositeAttributeDefinition>();
        }

        #endregion

////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Attributes
////////////////////////////////////////////////////////////////////////////////////////////////////////////
#region Attributes

 



        //There are three ways that you can add attributes
        //1: Passing the Attribute 
        //2: By supplying  type, value, name
        //3: By passing an AttributeDefinition

        //By Attribute
        public virtual void AddAttribute(CompositeAttribute attribute)
        {
            if (attribute == null)
                throw new Exception("Attribute can not be null");

            if (GetAttribute(attribute.Name, attribute.Type) == null)
                addChild(attribute);            

        }

        //By supplying  type, value, name
        public virtual void AddAttribute(String type, String value, String name = null)
        {

            if (GetAttribute(name, type) == null)
                addChild(new CompositeAttribute(name, type, value));

        }

        //By passing an AttributeDefinition
        public virtual void AddAttribute(CompositeAttributeDefinition attDef)
        {

            if (GetAttribute(attDef.Name, attDef.Type) == null)
                addChild(new CompositeAttribute(attDef.Name, attDef.Type, attDef.Value) { WriteToDisk = attDef.WriteToDisk });            
                

        }



        //Removes a composite's attribute by passing name and type
        public virtual void RemoveAttribute(String name, String type)
        {            
                removeChild(GetAttribute(name, type));
        }


        //Removes a composite's attribute by passing the attribute to be removed.
        public virtual void RemoveAttribute(CompositeAttribute attribute) { removeChild(attribute); }


        //Sets an attribute's value
        public virtual int SetAttribute(String name, String type, String value)
        {
            CompositeAttribute att = GetAttribute(name, type);

            if (att == null)
                return -1;
            else
                att.Value = value;

            return 1;            

        }


        //Returns an attribute based on the name and type
        public CompositeAttribute GetAttribute(String name, String type = null)
        {

            if (String.IsNullOrEmpty(type))
            {
                return this.Attributes.Where(e => e.Name == name).FirstOrDefault();
            }
            else
                return this.Attributes.Where(e => e.Type == type && e.Name == name).FirstOrDefault();
        }



        //Initialize the composite's attributes based in the composite's attribute definitions
        public virtual void InitializeAttributes()
        {

            foreach(CompositeAttributeDefinition attDef in AttributeDefinitions)
                AddAttribute(attDef);
      
        }


#endregion

////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Other
////////////////////////////////////////////////////////////////////////////////////////////////////////////

        #region other
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///Qualifiers
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public List<Qualifier> Qualifiers { get { return getSkipList<Qualifier>(); } }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///Subject Functions
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
        private List<IComposite> _observers;

        public virtual void Attach(IComposite observer)
        {
            try
            {
                foreach (Composite obj in _observers)
                    if (obj.ObjectID == ((Composite)observer).ObjectID)
                        throw new Exception("Object Exist");

                _observers.Add(observer);

            }
            catch (Exception e) { }
        }

        public virtual void Detach(IComposite observer)
        {
            try
            {
                foreach (Composite obj in _observers)
                    if (obj.ObjectID == ((Composite)observer).ObjectID)
                    {
                        _observers.Remove(observer);
                        return;
                    }
                throw new Exception("Object Does Not Exist");

            }
            catch (Exception e) { }
        }

        public virtual void Notify()
        {
            foreach (Composite ob in _observers)
                ob.Update(this);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///Observer Functions
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public virtual void Update(IComposite subject)
        {

        }


        #endregion

    }
}
